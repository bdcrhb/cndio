import sys
import os
sys.path.append('lib')
sys.path.append('cnd_io')
import cnd_io
from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import cndprint


_print = cndprint.CndPrint(level="Info", silent_mode=True)
source_name = "MySource"
file_name = "MyPath/FileName"
creds = {'abc': 'def'}

with description("CndProvider") as self:
    with before.each:
        unstub()
        self.io = cnd_io.CndProvider(creds=creds, print=_print)

    with context("__init__"):
        with it("should set creds"):
            expect(self.io._creds).to(equal(creds))

    with context("print_me"):
        with it("should return False and use default printing if self.print is none"):
            self.io = cnd_io.CndProvider(creds=creds)
            expect(self.io._print_me('', '', silent_mode=True)).to(equal(False))

        with it("should return True and use CndPrint"):
            expect(self.io._print_me('info_e', '')).to(equal(True))

    with context("pull_file"):
        with it("should return None"):
            expect(self.io.pull_file('', '')).to(equal(None))

    with context("push_file"):
        with it("should return True"):
            expect(self.io.push_file('', '', '')).to(equal(True))

    with context("push_files"):
        with it("should return True"):
            expect(self.io.push_files('', [])).to(equal(True))    

    with context("file_exist"):
        with it("should return True"):
            expect(self.io.file_exist('', '')).to(equal(True))                 