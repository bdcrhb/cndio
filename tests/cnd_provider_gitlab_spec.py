import sys
import os
sys.path.append('lib')
sys.path.append('cnd_io')
import cnd_io
from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import cndprint
import requests
import json


_print = cndprint.CndPrint(level="Trace", silent_mode=True)
creds = {'private_token': os.environ['PRIVATE_TOKEN'], 'host': "https://gitlab.com/api/v4/"}
source_name = "tests_content"
file_name = "README.md"
file_content = 'ABC'
project_id = "40457945"
branch = "main"
blob_id = "4acecf298d651473e9b2ff56a4b4d504dca9cbd2"

with description("CndProviderGitlab") as self:
    with before.each:
        unstub()
        self.io = cnd_io.CndProviderGitlab(creds=creds, print=_print)

    with context("__init__"):
        with it("shoud init value"):
            expect(self.io._host).to(equal(creds["host"]))

    with context("_query"):
        with it("shoud raise an error if method is not allowed"):
            expect(lambda: self.io._query("", method="toto")).to(raise_error(AttributeError))


    with context("_query"):
        with it("shoud accept get by default and load the json"):
            response = mock({'content': '{"a": "b"}', 'status_code': 200}, spec=requests.Response)
            when(requests).get(...).thenReturn(response)
            result = self.io._query('url')
            expect(json.loads(result.content)).to(equal({"a": "b"}))

        with it("shoud return False if result is not good"):
            response = mock({'content': '{"a": "b"}', 'status_code': 500}, spec=requests.Response)
            when(requests).get(...).thenReturn(response)
            result = self.io._query('url')
            expect(result).to(equal(False))

        with it("shoud return False if result is not good"):
            response = mock({'content': '{"a": "b"}', 'status_code': 201}, spec=requests.Response)
            when(requests).post(...).thenReturn(response)
            result = self.io._query('url', method="post")
            expect(json.loads(result.content)).to(equal({"a": "b"}))

    with context("pull_file"):
        with it("should return File"):
            when(self.io)._get_blob_id(...).thenReturn('')
            when(self.io)._get_blob(...).thenReturn(file_content)
            file = self.io.pull_file(project_id, file_name)
            expect(file).to(equal(file_content))

        with it("should return None"):
            when(self.io)._get_blob_id(...).thenReturn(None)
            when(self.io)._get_blob(...).thenReturn(file_content)
            file = self.io.pull_file(project_id, file_name)
            expect(file).to(equal(None))

    with context("_get_blob_id"):
        with it("should return raw file blob id"):
            response = mock({'content': json.dumps({"blob_id": blob_id}), 'status_code': 200}, spec=requests.Response)
            when(self.io)._query(...).thenReturn(response)
            result = self.io._get_blob_id(file_name, branch)
            expect(result).to(equal(blob_id))

        with it("should return raw file blob id"):
            when(self.io)._query(...).thenReturn(False)
            result = self.io._get_blob_id(file_name, branch)
            expect(result).to(equal(False))

        with it("should return None if file didn't exists"):
            response = mock({'content': json.dumps({"blob_id": blob_id}), 'status_code': 404}, spec=requests.Response)
            when(self.io)._query(...).thenReturn(response)
            result = self.io._get_blob_id(file_name, branch)
            expect(result).to(equal(None))

    with context("_get_blob"):
        with it("should return raw file blob id"):
            response = mock({'content': file_content.encode(), 'status_code': 200}, spec=requests.Response)
            when(self.io)._query(...).thenReturn(response)
            file = self.io._get_blob(blob_id)
            expect(file).to(equal(file_content))

        with it("should return raw file blob id"):
            when(self.io)._query(...).thenReturn(False)
            file = self.io._get_blob(blob_id)
            expect(file).to(equal(False))

    with context("push_files"):
        with it("should return succes if file didn't exist"):
            response = mock({'status_code': 201}, spec=requests.Response)
            when(self.io)._query(...).thenReturn(response)
            when(self.io).file_exist(...).thenReturn(None)
            when(self.io)._get_blob(...).thenReturn('ABC')
            files = {'A.txt': 'DEF'}
            file = self.io.push_files(project_id, files, "This is a nice commit")
            expect(file).to(equal(True))

        with it("should return None if file exist and it's the same"):
            response = mock({'status_code': 201}, spec=requests.Response)
            when(self.io)._query(...).thenReturn(response)
            when(self.io).file_exist(...).thenReturn(123)
            when(self.io)._get_blob(...).thenReturn('DEF')
            files = {'A.txt': 'DEF'}
            file = self.io.push_files(project_id, files, "This is a nice commit")
            expect(file).to(equal(None))

        with it("should return success if file exist and it's not the same"):
            response = mock({'status_code': 201}, spec=requests.Response)
            when(self.io)._query(...).thenReturn(response)
            when(self.io).file_exist(...).thenReturn(123)
            when(self.io)._get_blob(...).thenReturn('ABC')
            files = {'A.txt': 'DEF'}
            file = self.io.push_files(project_id, files, "This is a nice commit")
            expect(file).to(equal(True))

        with it("should return False"):
            when(self.io)._query(...).thenReturn(False)
            when(self.io).file_exist(...).thenReturn(123)
            when(self.io)._get_blob(...).thenReturn('ABC')
            files = {'A.txt': 'DEF'}
            file = self.io.push_files(project_id, files, "This is a nice commit")
            expect(file).to(equal(False))

    with context("file_exist"):
        with it("should return blob_id if file exist"):
            when(self.io)._get_blob_id(...).thenReturn(blob_id)
            result = self.io.file_exist(file_name, branch)
            expect(result).to(equal(blob_id))

    with context("push_file"):
        with it("should return True if file push_files success"):
            when(self.io).push_files(...).thenReturn(True)
            result = self.io.push_file(project_id, 'A.txt', 'DEF', branch=branch)
            expect(result).to(equal(True))

        with it("should return None if push_files None"):
            when(self.io).push_files(...).thenReturn(None)
            result = self.io.push_file(project_id, 'A.txt', 'DEF', branch=branch)
            expect(result).to(equal(None))
        with it("should return False if file push_files False"):
            when(self.io).push_files(...).thenReturn(False)
            result = self.io.push_file(project_id, 'A.txt', 'DEF', branch=branch)
            expect(result).to(equal(False))
