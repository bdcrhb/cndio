import sys
import os
sys.path.append('lib')
sys.path.append('cnd_io')
import cnd_io
from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import cndprint


_print = cndprint.CndPrint(level="Info", silent_mode=True)
provider = cnd_io.CndProviderLocalfile(print=_print)
source_name = "tests_content"
branch = "main"
file_name = "sample.txt"
file_content_1 = 'def'
file_content_0 = 'abc'
yaml_content = {'abc': {'def': 'ghi'}}
yaml_content_text = "{'abc': {'def': 'ghi'}}"

with description("CndIO") as self:
    with before.each:
        unstub()
        when(provider).pull_file(...).thenReturn(file_content_0)
        when(provider).push_file(...).thenReturn(True)
        when(provider).file_exist(...).thenReturn(True)
        self.io = cnd_io.CndIO(provider, print=_print)

    with context("__init__"):
        with it("should initiate cached_file"):
            expect(self.io._cached_file).to(equal({}))
            expect(self.io._provider).to(equal(provider))
            expect(self.io._print).to(equal(_print))

    with context("pull_file"):
        with it("should read and return file content"):
            expect(self.io.pull_file(source_name, file_name)).to(equal(file_content_0))

        with it("should return False if file is not existing"):
            when(self.io._provider).file_exist(...).thenReturn(False)
            expect(self.io.pull_file(source_name, f"{file_name}.toto")).to(equal(False))

    with context("push_file"):
        with it("should return True if pushing file work"):
            expect(self.io.push_file(source_name, file_name, file_content_1)).to(equal(True))

        with it("should return False if pushing content failed"):
            when(self.io._provider).push_file(...).thenReturn(False)
            expect(self.io.push_file(source_name, file_name, file_content_1)).to(equal(False))

    with context("pull_yaml_file"):
        with before.each:
            when(self.io._provider).pull_file(...).thenReturn(yaml_content_text)

        with it("should read and return file content"):
            when(self.io._provider).file_exist(...).thenReturn(False)
            expect(self.io.pull_yaml_file(source_name, file_name)).to(equal(False))

        with it("should read and return file content"):
            expect(self.io.pull_yaml_file(source_name, file_name)).to(equal(yaml_content))

    with context("push_yaml_file"):
        with it("should return True if pushing file work"):
            expect(self.io.push_yaml_file(source_name, file_name, yaml_content, branch=branch)).to(equal(True))       

    with context("commit_file"):
        with it("should return True if commit work"):
            result = self.io.commit_file(source_name, file_name, file_content_1)
            result = self.io.commit_file(source_name, f'1{file_name}', file_content_1)
            result = self.io.commit_yaml_file(source_name, f'2{file_name}', yaml_content)
            expect(result).to(equal(3))

    with context("push_files"):
        with it("should return None if not are commit for this source name"):
            when(self.io._provider).push_files(...).thenReturn(True)
            expect(self.io.push_files(source_name)).to(equal(None))

        with it("should return True if push work fine"):
            self.io._files[source_name] = {}
            when(self.io._provider).push_files(...).thenReturn(True)
            expect(self.io.push_files(source_name)).to(equal(None))

        with it("should return True if push work fine"):
            self.io._files[source_name] = {branch: {}}
            when(self.io._provider).push_files(...).thenReturn(True)
            expect(self.io.push_files(source_name)).to(equal(True))

        with it("should return False if push generate an error"):
            self.io._files[source_name] = {branch: {}}
            when(self.io._provider).push_files(...).thenReturn(False)
            expect(self.io.push_files(source_name)).to(equal(False))