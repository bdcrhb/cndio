import sys
import os
sys.path.append('lib')
sys.path.append('cnd_io')
import cnd_io
from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import cndprint


_print = cndprint.CndPrint(level="Info", silent_mode=True)
source_name = "tests_content"
file_name = "sample.txt"
creds = {'abc': 'def'}
file_content_1 = 'def'
file_content_0 = 'abc'

with description("CndProviderLocalfile") as self:
    with before.each:
        unstub()
        with open(f'{source_name}/{file_name}', 'w') as f:
            f.write(file_content_0)
        self.io = cnd_io.CndProviderLocalfile(creds=creds, print=_print)

    with context("pull_file"):
        with it("should return None"):
            expect(self.io.pull_file(source_name, file_name)).to(equal(file_content_0))

    with context("push_file"):
        with it("should return True"):
            expect(self.io.push_file(source_name, file_name, file_content_1)).to(equal(True))  
            expect(self.io.pull_file(source_name, file_name)).to(equal(file_content_1))  

    with context("push_files"):
        with it("should return True"):
            files = {
                        file_name: file_content_1
                    }
            
            expect(self.io.push_files(source_name, files)).to(equal(True))               

    with context("file_exist"):
        with it("should return True"):
            expect(self.io.file_exist(source_name, file_name)).to(equal(True))            

        with it("should return False"):
            expect(self.io.file_exist(source_name, 'toto')).to(equal(False))              